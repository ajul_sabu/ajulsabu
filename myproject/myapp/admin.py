from django.contrib import admin
from.models import category, student, teachers, product, user, task, Book, Publications
# Register your models here.
admin.site.register(student)
admin.site.register(teachers)
admin.site.register(category)
admin.site.register(product)
admin.site.register(user)
admin.site.register(task)
admin.site.register(Publications)
admin.site.register(Book)