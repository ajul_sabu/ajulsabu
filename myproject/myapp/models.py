from django.db import models

# Create your models here.
class student(models.Model):
    fname=models.CharField(max_length=40,null=True)
    lname=models.CharField(max_length=40,null=True)
    dob=models.DateField(null=True)
    email=models.EmailField(max_length=30,null=True)
    gen=models.CharField(choices=(('Male','Male'),('Female','Female')),null=True,max_length=30)
    sub=models.CharField(max_length=40,null=True)
    mob=models.IntegerField(null=True)
    u=models.CharField(max_length=10,null=True)
    psswd=models.CharField(max_length=12,null=True)


class teachers(models.Model):
    fn=models.CharField(max_length=40,null=True)
    ln=models.CharField(max_length=40,null=True)
    d=models.DateField(null=True)
    ema=models.EmailField(max_length=30,null=True)
    ge=models.CharField(choices=(('Male','Male'),('Female','Female')),null=True,max_length=30)
    br=models.CharField(max_length=40,null=True)
    pho=models.IntegerField(null=True)
    m1=models.IntegerField(null=True)
    m2=models.IntegerField(null=True)
    profile_pic=models.ImageField(upload_to='image/',null=True)

class category(models.Model):
    cname=models.CharField(max_length=40)
    def __str__(self):
        return self.cname

class product(models.Model):
    pname=models.CharField(max_length=30,null=True)
    price=models.IntegerField(null=True)
    cat=models.ForeignKey('category', on_delete=models.CASCADE,null=True)

    def __str__(self):
        return self.pname

class user(models.Model):
    user_name=models.CharField(max_length=20,null=True)
    email=models.EmailField(max_length=40,null=True)
    password=models.CharField(max_length=20,null=True) 
    def __str__(self):
        return self.user_name

class task(models.Model):
    task_name=models.CharField(max_length=30,null=True)
    date=models.DateField(null=True)
    us=models.OneToOneField(user,on_delete=models.CASCADE,null=True) 

    def __str__(self):
        return self.task_name  



class Publications(models.Model):
    publication=models.CharField(max_length=30,null=True)
    def __str__(self):
        return self.publication 

class Book(models.Model):
    book_name=models.CharField(max_length=30,null=True)
    publication_name=models.ManyToManyField(Publications)
    def __str__(self):
        return self.book_name


      