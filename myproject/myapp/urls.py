from django.urls import path
from .views import *
from MyApp import views
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('',views.new,name="h"),
    path('home',addstudent,name="add"),
    path('getall',views.getuser,name="all"),
    path('mfilter',views.markfilter,name="mark"),
    path('del<int:userid>',views.deluser,name="dl"),
    path('edit<int:userid>',views.update_user,name="ed"),
    path('log',views.login_user,name="login"),
    path('get',views.teach,name="st"),
    path('lout',views.logout_user,name="logout"),
]+static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root = settings.STATIC_URL)
    urlpatterns += static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)